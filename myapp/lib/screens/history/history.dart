import 'package:app/models/History.dart';
import 'package:app/screens/history/foodItem.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:geocoder/geocoder.dart';
import 'package:provider/provider.dart';
class HistoryPage extends StatefulWidget {
  static const routeName = '/history';
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  Coordinates coordsVillechoisie;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    ScreenUtil.init(context, height: 760, width: 360, allowFontScaling: false);

    return SafeArea(
      child: Column(
        children: <Widget>[
          Expanded(flex: 1,child:
            Container(
              padding: EdgeInsets.only(top: ScreenUtil().setSp(10), bottom: ScreenUtil().setSp(10),
                  right:  ScreenUtil().setSp(18)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                      height: ScreenUtil().setHeight(35),width: ScreenUtil().setWidth(35),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setSp(20))),
                          image: DecorationImage(image: ExactAssetImage("assets/hansP.JPG"),fit: BoxFit.cover)
                      ))
                ],),
            ),),
          Expanded(flex: 2,child:
            Container(
              padding: EdgeInsets.only(left: ScreenUtil().setSp(18), bottom: ScreenUtil().setSp(8),
                  top: ScreenUtil().setSp(8)),
              child: Column(
                children: <Widget>[
                  Expanded(flex: 1,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text("History", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold,
                            fontSize: ScreenUtil().setSp(25)),),
                      ],
                    ),),
                ],
              ),
            ),),
          Expanded(flex: 9,
            child: Container(
              padding: EdgeInsets.all(ScreenUtil().setSp(15)),
                child: Consumer<History>(
                  builder: (context, history, _){
                    if(history.orders != null && history.orders.length > 0){
                      return ListView.builder(
                        itemCount: history.orders.length,
                        scrollDirection: Axis.vertical,
                        itemBuilder: (context, i){
                          return FoodItem(order: history.orders[i],);
                        },
                      );
                    }else{
                      return Text("Nothing Here!", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold,
                          fontSize: ScreenUtil().setSp(25)),);
                    }
                  },
                )
            )
            ,),
        ],
      ),
    );
  }
}
