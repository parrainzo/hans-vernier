import 'package:app/config/colors.dart';
import 'package:app/helpers/Date_Converter.dart';
import 'package:app/models/Order.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:geocoder/geocoder.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
class FoodItem extends StatefulWidget {

  //Pour faire passer la commande plus facilement à la vue!
  final Order order;
  FoodItem({Key key, this.order}) : super(key:key);
  @override
  _FoodItemState createState() => _FoodItemState();
}

class _FoodItemState extends State<FoodItem> {

  @override
  void initState() {
    //appel de la fonction pour convetir la position en nom de lieu
    locationToStrng();
    super.initState();
  }

  String ville = "";
  String commune = "";
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    //Du ScreenUtil pour le responsive sur tout type d"écran.
    ScreenUtil.init(context, height: 760, width: 360, allowFontScaling: false);

    return ExpandablePanel(
        header: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(DateConverter().convert(widget.order.date.toString()), style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold,
                fontSize: ScreenUtil().setSp(25)),)
          ],
        ),
        expanded: Container(
          height: ScreenUtil().setHeight(165),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text("$ville, $commune", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold,
                      fontSize: ScreenUtil().setSp(15)),),
                  Icon(OMIcons.map, color: Colors.red,)
                ],
              ),
              SizedBox(height: ScreenUtil().setHeight(10),),
              Expanded(
                child: PageView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: widget.order.products.length,
                  itemBuilder: (context, z){
                    return Container(
                      margin: EdgeInsets.symmetric(vertical: ScreenUtil().setSp(5), horizontal: ScreenUtil().setSp(5)),
                      width: mediaQueryData.size.width -  ScreenUtil().setHeight(10),
                      height:  ScreenUtil().setHeight(165),
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.1),
                          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setSp(15)))
                      ),
                      child: Row(
                        children: <Widget>[
                          Expanded(flex: 1,child: Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: ScreenUtil().setSp(10), right: ScreenUtil().setSp(50), bottom: ScreenUtil().setSp(10)),
                                decoration: BoxDecoration(
                                    color: mPink,
                                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setSp(30)))
                                ),
                              ),
                              InkWell(onTap: (){
                              },
                                child:  Container(
                                  margin: EdgeInsets.only(left: ScreenUtil().setSp(15), right: ScreenUtil().setSp(10), bottom: ScreenUtil().setSp(40)),
                                  decoration: BoxDecoration(
                                      image: DecorationImage(image: ExactAssetImage(widget.order.products[z].imageUrl),fit: BoxFit.contain)
                                  ),
                                ),)
                            ],
                          ),),
                          Expanded(flex: 1,child: Padding(
                            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setSp(30)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("${widget.order.products[z].price} \$", style: TextStyle(color: mGreen, fontWeight: FontWeight.bold, fontSize:ScreenUtil().setSp(25)),),
                                Container(width: 110,child: Text("${widget.order.products[z].name}", style: TextStyle(color: mGreen, fontWeight: FontWeight.bold, fontSize:ScreenUtil().setSp(15)),),),
                              ],
                            ),
                          ),),
                        ],
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        )
    );
  }
  locationToStrng() async{

    //n vérifie si on a bien la position
    if(widget.order.longitude != null && widget.order.latitude != null){
      Coordinates coordinates = new Coordinates(widget.order.latitude, widget.order.longitude);
      final cityName = await Geocoder.local.findAddressesFromCoordinates(coordinates);
      setState(() {
        //On a assigne le resultat obtenu dans nos variables définies plus haut.
        ville = cityName.first.locality;
        commune = cityName.first.subLocality;
      });
      print("Nous sommes à ${cityName.first.addressLine}");
    }
  }
}
