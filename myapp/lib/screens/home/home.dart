import 'package:app/config/colors.dart';
import 'package:app/models/Category.dart';
import 'package:app/models/CategoryList.dart';
import 'package:app/models/History.dart';
import 'package:app/models/Order.dart';
import 'package:app/screens/home/dartItem.dart';
import 'package:app/screens/home/homeItem.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FoodHomePage extends StatefulWidget {
  static const routeName = '/home';
  @override
  _FoodHomePageState createState() => _FoodHomePageState();
}

class _FoodHomePageState extends State<FoodHomePage> {
  var _isLoading = false;
  var _isInit = true;
  List<Cat> cats = [];
  Location location;
  LocationData locationData;

  @override
  void initState() {
    // TODO: implement initState
    location = new Location();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<CategoryList>(context).fetchCats().then((data) {
        setState(() {
          _isLoading = false;
          cats = data;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    ScreenUtil.init(context, height: 760, width: 360, allowFontScaling: false);

    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(flex: 1,child:
              Container(
                padding: EdgeInsets.only(top: ScreenUtil().setSp(10), bottom: ScreenUtil().setSp(10),
                    right:  ScreenUtil().setSp(18)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                  Container(
                      height: ScreenUtil().setHeight(35),width: ScreenUtil().setWidth(35),
                      decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setSp(20))),
                    image: DecorationImage(image: ExactAssetImage("assets/hansP.JPG"),fit: BoxFit.cover)
                    ))
                ],),
              ),),
            Expanded(flex: 2,child:
              Container(
              padding: EdgeInsets.only(left: ScreenUtil().setSp(18), bottom: ScreenUtil().setSp(8),
                  top: ScreenUtil().setSp(8)),
                child: Column(
                  children: <Widget>[
                    Expanded(flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text("OderDelivery", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold,
                                fontSize: ScreenUtil().setSp(25)),),
                      ],
                    ),),
                    Expanded(flex: 2,
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: ScreenUtil().setSp(15)),
                        child: Row(
                          children: <Widget>[
                            ColorItem(myChild: Icon(OMIcons.swapHoriz, color: Colors.white,),val: ScreenUtil().setWidth(60),mColor: mBlue,),
                            Expanded(
                              child: _isLoading ?
                              SpinKitWave(color: mBlue, type: SpinKitWaveType.start, size: 50.0,):
                              ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: cats.length,
                                  itemBuilder: (ctx, i){
                                    return  ColorItem(myChild:
                                    Text(cats[i].name, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: ScreenUtil().setSp(15))),
                                      val: ScreenUtil().setWidth(80),mColor: mBlue,);
                                  }
                              ),
                            )
                          ],
                        )
                      ),
                    ),
                  ],
                ),
            ),),
            Expanded(flex: 9,child:
              Padding(padding: EdgeInsets.only(left: ScreenUtil().setSp(18)),
                child: Column(
                  children: <Widget>[
                  Expanded(flex: 1,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Category", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: ScreenUtil().setSp(20)),),
                      Padding(padding: EdgeInsets.only(right: ScreenUtil().setSp(18)),
                        child: Consumer<Order>(
                          builder: (context, order, _){
                            if(order.products != null && order.products.length > 0){
                              return RaisedButton(
                                onPressed: (){
                                  _displayDialog(context);
                                },
                                child: Text("Order now (${order.products.length})", style: TextStyle(color: Colors.white,
                                    fontWeight: FontWeight.bold, fontSize: ScreenUtil().setSp(10)),),
                                color: mBlue,
                              );
                            }else{
                              return AbsorbPointer(
                                absorbing: true,
                                child: RaisedButton(
                                  onPressed: (){

                                  },
                                  child: Text("Order now", style: TextStyle(color: Colors.white,
                                      fontWeight: FontWeight.bold, fontSize: ScreenUtil().setSp(10)),),
                                  color: Colors.grey,
                                ),
                              );
                            }
                          },
                        ),)
                    ],
                  ),),
                    Expanded(flex: 10,
                      child: Container(
                        child: ListView.builder(
                          itemBuilder: (context, int i){
                            return Container(
                              width: ScreenUtil().setWidth(280),
                              child: _isLoading ? Center(
                                child: SpinKitRing(color: mPink, size: 300.0,),
                              ):
                              ListView.builder(
                                scrollDirection: Axis.vertical,
                                itemCount: cats[i].products.length,
                                itemBuilder: (context, z){
                                  return HomeItem(product: cats[i].products[z],);
                                },
                              ),

                            );
                          },
                          itemCount: cats.length,
                          scrollDirection: Axis.horizontal,
                        ),
                      ),)
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Pop up pour le choix des positions - maps ou position actuelle
  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return Dialog(
              elevation: 0.0,
              child: InkWell(
                splashColor: Colors.grey.withOpacity(0.3),
                onTap: (){
                },
                child: Container(
                  padding: EdgeInsets.only(left: ScreenUtil().setSp(15), right: ScreenUtil().setSp(15),),
                  width: MediaQuery.of(context).size.width,
                  height: ScreenUtil().setHeight(65),
                  alignment: Alignment.center,
                  child:  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      //Partie position actuelle
                      RaisedButton(
                        onPressed: (){
                          getPosition();
                        },
                        child: Text("Utiliser ma position", style: TextStyle(color: Colors.white,
                            fontWeight: FontWeight.bold, fontSize: ScreenUtil().setSp(10)),),
                        color: mBlue,
                      ),
                      //Partie maps
                      RaisedButton(
                        onPressed: (){
                          Navigator.of(context).pop();
                          Navigator.pushNamed(context, "/maps");
                        },
                        child: Text("Utiliser la maps", style: TextStyle(color: Colors.white,
                            fontWeight: FontWeight.bold, fontSize: ScreenUtil().setSp(10)),),
                        color: mBlue,
                      )
                    ],
                  ),
                ),
              )
          );
        });
  }

  //Fonction pour récupérer la position actuelle du User
  getPosition() async{
    try {
      locationData = await location.getLocation();
      Provider.of<Order>(context, listen: false).addPosition(locationData.longitude, locationData.latitude);
      print("Position actuelle..............: ${locationData.latitude} / ${locationData.longitude}");
      Navigator.of(context).pop();

      //Appel du toas de réussite
     await Fluttertoast.showToast(
          msg: "Order successed!",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: mGreen,
          textColor: Colors.white,
          fontSize: 16.0
      );
     //Liste d'action du provider: Ajout d'une comande et réinitialisation du panier
      var order = Provider.of<Order>(context, listen: false).toMap();
      await Provider.of<History>(context, listen: false).addOrder(order);
      print(Provider.of<History>(context, listen: false).orders[0].products);
      await Provider.of<Order>(context, listen: false).reset();
      //Retour sur la même page pour annulation du SetState
      Navigator.pushNamed(context, "/bottom");

    }catch (e){
      print("ERROR------------${e}");
    }
  }
}


