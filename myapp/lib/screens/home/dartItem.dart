import 'package:flutter/material.dart';


class ColorItem extends StatefulWidget {

  final Widget myChild;
  Color mColor;
  double val;

  ColorItem({Key key, this.myChild, this.mColor, this.val}) : super(key:key);

  @override
  _ColorItemState createState() => _ColorItemState();
}

class _ColorItemState extends State<ColorItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(horizontal: 5),
      width: widget.val,
      decoration: BoxDecoration(
        color: widget.mColor,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), bottomRight: Radius.circular(20), bottomLeft: Radius.circular(5), topRight: Radius.circular(5))
      ),
      child: widget.myChild,
    );
  }
}
