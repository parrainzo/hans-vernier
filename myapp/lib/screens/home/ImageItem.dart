import 'package:flutter/material.dart';

class ImageItem extends StatefulWidget {
  final String image;
  ImageItem({Key key, this.image}) : super(key:key);

  @override
  _ImageItemState createState() => _ImageItemState();
}

class _ImageItemState extends State<ImageItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      width: 100,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: ExactAssetImage(widget.image),
          fit: BoxFit.contain,
        ),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(35), bottomRight: Radius.circular(35), bottomLeft: Radius.circular(5), topRight: Radius.circular(5))
      ),
    );
  }
}
