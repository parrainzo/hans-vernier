import 'dart:math';

import 'package:app/config/colors.dart';
import 'package:app/models/Order.dart';
import 'package:app/models/Product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:provider/provider.dart';

class HomeItem extends StatefulWidget {
  final Product product;

  HomeItem({Key key, this.product}) : super(key:key);

  @override
  _HomeItemState createState() => _HomeItemState();
}

class _HomeItemState extends State<HomeItem> {
  List<Color> colors = [
    mPink,
    mPink,
    mBlue
  ];
  Random rnd = new Random();
  bool isOrdered = false;

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    ScreenUtil.init(context, height: 760, width: 360, allowFontScaling: false);
    int r = 0 + rnd.nextInt(2 - 0);
    return  Container(
        margin: EdgeInsets.symmetric(vertical: ScreenUtil().setSp(2), horizontal: ScreenUtil().setSp(5)),
        height: ScreenUtil().setHeight(200),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(ScreenUtil().setSp(16)),
          boxShadow: [buildBoxShadow(mPink, context)],
        ),
        child: Material(
          borderRadius: BorderRadius.circular(ScreenUtil().setSp(16)),
          clipBehavior: Clip.antiAlias,
          color: isOrdered ? mPink : Theme.of(context).dialogBackgroundColor,
          child: InkWell(
            borderRadius: BorderRadius.circular(ScreenUtil().setSp(16)),
            onTap: () {
              if(isOrdered){
                Provider.of<Order>(context, listen: false).deleteProduct(widget.product.id);
              }else{
                Provider.of<Order>(context, listen: false).addProduct(widget.product);
              }
             setState(() {
               isOrdered =! isOrdered;
             });
            },
            splashColor: colors[r],
            highlightColor: mPink,
            child: Container(
              width: ScreenUtil().setWidth(100),
              margin: EdgeInsets.only(top: ScreenUtil().setSp(5),bottom: ScreenUtil().setSp(30), right: ScreenUtil().setSp(20)),
              decoration: BoxDecoration(
                  boxShadow: [buildBoxShadow(mPink, context)],
                  color: mPink,
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setSp(20)))
              ),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: ScreenUtil().setSp(60), bottom: ScreenUtil().setSp(30)),
                    padding: EdgeInsets.only(left: ScreenUtil().setSp(20)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text("${widget.product.price} \$", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: ScreenUtil().setSp(25)),),
                          ],
                        ),
                        Container(width: 150,child:
                          Text("${widget.product.name}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: ScreenUtil().setSp(15)),)
                        )

                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: ScreenUtil().setSp(10), left: ScreenUtil().setSp(60), top: ScreenUtil().setSp(20)),
                    padding: EdgeInsets.only(bottom: ScreenUtil().setSp(20)),
                    height: double.infinity,
                    width: double.infinity,
                    child: Image.asset("${widget.product.imageUrl}", fit: BoxFit.contain,),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: ScreenUtil().setSp(340),top: ScreenUtil().setSp(5), left: ScreenUtil().setSp(170), ),
                    height: double.infinity,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.7),
                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20), topRight: Radius.circular(20))
                    ),
                    child: Icon(OMIcons.favoriteBorder, color: Colors.white,),
                  ),
                ],
              ),
            ),
          ),
        ));
  }


  BoxShadow buildBoxShadow(Color color, BuildContext context) {
    return BoxShadow(
        color: color.withAlpha(60),
        blurRadius: 20,
        offset: Offset(0, 8));
  }

  void toNewPage(Widget page){
    Navigator.push(context, CupertinoPageRoute(builder: (BuildContext context){
      return page;
    }));
  }
}
