import 'package:app/config/colors.dart';
import 'package:app/screens/history/history.dart';
import 'package:app/screens/home/home.dart';
import 'package:flutter/material.dart';
import 'package:outline_material_icons/outline_material_icons.dart';

class bottomNavigationBar extends StatefulWidget {
  static const routeName = '/bottom';

  @override
  _bottomNavigationBarState createState() => _bottomNavigationBarState();
}

class _bottomNavigationBarState extends State<bottomNavigationBar> {
  int currentIndex = 0;
  Widget callPage(int current) {
    switch (current) {
      case 0:
        return new FoodHomePage();
      case 1:
        return new HistoryPage();
      default:
        return FoodHomePage();
    }
  }

  /// Build BottomNavigationBar Widget
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: callPage(currentIndex),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 0,
        backgroundColor: Colors.white,
        fixedColor: mBlue,
        currentIndex: currentIndex,
        onTap: (value) {
          currentIndex = value;
          setState(() {});
        },
        items: [
          BottomNavigationBarItem(icon: Icon(OMIcons.home,), title: Text("")),
          BottomNavigationBarItem(icon: Icon(OMIcons.history), title: Text("")),
        ],),
    );
  }
}

