import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:math' as math;
import 'package:app/config/colors.dart';
import 'package:app/models/History.dart';
import 'package:app/models/Order.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'widgets/toolbar.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'map_utils.dart';

class MapsPage extends StatefulWidget {
  static const routeName = '/maps';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<MapsPage> {
  PanelController _panelController  = PanelController() ;
  GoogleMapController _mapController;
  Timer _idleTimer ;
  CameraPosition _initialCameraPosition ;
  StreamSubscription<Position> _positionStream;
  Map<MarkerId, Marker> _markers = Map();
  Map<PolylineId, Polyline> _polylines = Map();
  Map<PolygonId, Polygon> _polygons = Map();
  bool _isPanelOpen = false ;
  LatLng _centerPosition , _myPosition ;



  @override
  void initState() {
    super.initState();
    _startTracking();

  }

  _startTracking() {
    final geolocator = Geolocator();
    final locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 5);

    _positionStream = geolocator.getPositionStream(locationOptions).listen(_onLocationUpdate);
    print("geop:$_positionStream");
  }

  _onLocationUpdate(Position position) {
    if (position != null) {
      if(_initialCameraPosition == null) {
        final myPosition = LatLng(position.latitude,position.longitude);
        _myPosition = myPosition ;
        print("myPositon : $myPosition");
    setState(() {
      _initialCameraPosition = CameraPosition(target: myPosition, zoom: 14);
    });
      }
      final myPosition = LatLng(position.latitude, position.longitude);

    }
  }

  @override
  void dispose() {
    _idleTimer?.cancel();
    if (_positionStream != null) {
      _positionStream.cancel();
      _positionStream = null;
    }


    super.dispose();
  }

  _moveCamera(LatLng position,{ double zoom = 12}) {
    final cameraUpdate = CameraUpdate.newLatLngZoom(position,zoom);
    _mapController.animateCamera(cameraUpdate);
  }

  _onCameraMoveStarted() {
  print("move started");

  }

  _onCameraMove(CameraPosition cameraPosition) {
    print("position , longitude ${cameraPosition.target.longitude}, latitude :${cameraPosition.target.latitude}");
    _centerPosition = cameraPosition.target ;
  }




  _onGoMyPosition() {
    if(_myPosition !=  null) {
      _moveCamera(_myPosition,zoom: 15);
    }
  }

  _onTap(LatLng p) async{
    print("position tape : ${p.latitude} ${p.longitude}");
    Provider.of<Order>(context, listen: false).addPosition(p.longitude, p.latitude);
    var order = Provider.of<Order>(context, listen: false).toMap() ;
    await Provider.of<History>(context, listen: false).addOrder(order);
    print(Provider.of<History>(context, listen: false).orders);
    await Fluttertoast.showToast(
        msg: "Order successed!",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: mGreen,
        textColor: Colors.white,
        fontSize: 16.0
    );
    await Provider.of<Order>(context, listen: false).reset();
    Navigator.pushNamed(context, "/bottom");
    final markerId = MarkerId("${_markers.length}");
    final marker = Marker(markerId: markerId,position: p);
    setState(() {
      _markers = new Map() ;
      _markers[markerId] = marker ;
    });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size ;
    final padding =  MediaQuery.of(context).padding ;
    final slidingUpPanelHeight = size.height - padding.top - padding.bottom ;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: _initialCameraPosition== null
            ?  Center(child: CupertinoActivityIndicator(radius: 15,),)
            : SafeArea(child:  Column(
          children: <Widget>[
            Expanded(
                child: Stack(
                  children: <Widget>[
                    GoogleMap(
                      initialCameraPosition: _initialCameraPosition,
                      myLocationButtonEnabled: true,
                      myLocationEnabled: true,
                      markers: Set.of(_markers.values),
                      polylines: Set.of(_polylines.values),
                      polygons:Set.of(_polygons.values),
                      onCameraMoveStarted: _onCameraMoveStarted,
                      onLongPress: _onTap,
                      onCameraMove: _onCameraMove ,
//                            onCameraIdle: _onCameraIdle ,
                      onMapCreated: (GoogleMapController controller) {
                        _mapController = controller;
                        _mapController.setMapStyle(jsonEncode(mapStyle));
                      },
                    ),
                    Tooblbar()

                  ],
                )
            ),

          ],
        )),
      ),
    );
  }
}
