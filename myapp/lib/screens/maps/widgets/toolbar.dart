import 'package:app/config/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:outline_material_icons/outline_material_icons.dart';


class Tooblbar extends StatefulWidget {
  final double containerHeigth ;
  final VoidCallback onGoMyPosition, onClear ;

  const Tooblbar({Key key, this.onGoMyPosition, this.containerHeigth, this.onClear}) : super(key: key);
  @override
  _TooblbarState createState() => _TooblbarState();
}

class _TooblbarState extends State<Tooblbar> {

  var query = "";


 final TextEditingController _textEditingController = TextEditingController() ;

  _onChanged(String text)async {
    query = text ;
    setState(() {});

    if(text.trim().length > 0) {
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }


  @override
  void dispose() {
    // TODO: implement dispose

    _textEditingController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final isNotEmpty = query.trim().length >0 ;
    return Positioned(
         left: 10,
        right: 10,
        top: 10,

        child: SafeArea(

          child: Container(
            decoration: BoxDecoration(color: mBlue,borderRadius: BorderRadius.circular(10)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Padding(padding: EdgeInsets.only(left: 20),
                    child: CupertinoTextField(
                    placeholder: 'Search',
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    style: TextStyle(color: Colors.white),
                    decoration: BoxDecoration(
                        color: Colors.transparent
                    ),
                  ),),
                ),
                CupertinoButton(
                  padding: EdgeInsets.all(10),
                  onPressed: () {
                  },
                  child: Icon(OMIcons.map, color: Colors.red,),
                )
              ],
            ),
          ),
        ));
  }
}
