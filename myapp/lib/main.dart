
import 'package:app/models/History.dart';
import 'package:app/screens/history/history.dart';
import 'package:app/screens/home.dart';
import 'package:app/screens/home/home.dart';
import 'package:app/screens/maps/index.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'models/CategoryList.dart';
import 'models/Order.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);


    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: CategoryList(),
        ),
        ChangeNotifierProvider.value(
          value: Order(),
        ),
        ChangeNotifierProvider.value(
          value: History(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        debugShowCheckedModeBanner: false,
        home: bottomNavigationBar(),
        routes: {
          FoodHomePage.routeName: (ctx) => FoodHomePage(),
          MapsPage.routeName: (ctx) => MapsPage(),
          HistoryPage.routeName: (ctx) => HistoryPage(),
          bottomNavigationBar.routeName: (ctx) => bottomNavigationBar(),
        },
      ),
    );
  }
}
