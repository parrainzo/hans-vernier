import 'package:flutter/material.dart';

Color mDark = Color.fromRGBO(27, 27, 37, 1);
Color mGreen = Color.fromRGBO(43, 200, 123, 1);
Color mGreen2 = Color.fromRGBO(202, 236, 222, 1);
Color mOrange = Color.fromRGBO(244, 195, 116, 1);
Color mPink = Color.fromRGBO(254, 184, 148, 1);
Color mBlue = Color.fromRGBO(52, 181, 230, 1);
Color mGrey = Color.fromRGBO(239, 240, 242, 1);