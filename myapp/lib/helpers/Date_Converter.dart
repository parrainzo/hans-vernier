import 'package:intl/intl.dart';
class DateConverter{
  String convert(String date){
    var formater = DateFormat('yyyy-MM-dd HH:mm:ss');
    DateTime dateTime = formater.parse(date);
    if(dateTime == null){
      return "Date inconnue";
    }else{
      var difference = DateTime.now().difference(dateTime);
      var days = difference.inDays;
      var hours = difference.inHours;
      var minutes = difference.inMinutes;
      var secondes = difference.inSeconds;
      if(days > 1){
        return "Il y'a $days jours";
      }else if(days == 1){
        return "Il y'a $days jour";
      }else{
        if(hours > 1){
          return "Il y'a $hours heures";
        }else if(hours == 1){
          return "Il y'a $hours heure";
        }else{
          if(minutes > 1){
            return "Il y'a $minutes minutes";
          }else if(minutes == 1){
            return "Il y'a $minutes minute";
          }else{
            if(secondes > 1){
              return "Il y'a $secondes secondes";
            }else if(secondes == 1){
              return "Il y'a $secondes seconde";
            }else{
              return "A l'instant";
            }
          }
        }
      }
    }
  }
}