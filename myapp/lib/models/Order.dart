import 'package:flutter/foundation.dart';
import 'package:app/models/Product.dart';

class Order with ChangeNotifier{
  String id;
  double longitude;
  double latitude;
  List<Product> _products = [];
  DateTime date;

  Order({
    this.id,
    this.longitude,
    this.latitude,
    this.date,
  });

  //SETTER ET GETTER pour notre liste de produit ici en privé
  List<Product> get products {
    return [..._products];
  }
  set setProducts(dta) {
    _products = dta;
  }

  //Ajout de la position
  addPosition(double log, double lat){
    this.latitude = lat;
    this.longitude = log;
    notifyListeners();
  }

  //Ajout des produits
  addProduct(Product product) {
    _products.add(product);
    // _items.insert(0, newProduct); // at the start of the list
    notifyListeners();
  }

  //Suppression d'un produit
  deleteProduct(int id) async {
    final existingProductIndex = _products.indexWhere((prod) => prod.id == id);
    _products.removeAt(existingProductIndex);
    notifyListeners();
  }
  Map<String, dynamic> toMap() => {
    "id": DateTime.now().toString(),
    "longitude": longitude,
    "latitude": latitude,
    "date": DateTime.now(),
    "products": _products.map((e) => e.toMap()).toList(),
  };

  factory Order.fromMap(Map<String, dynamic> json){
   Order ord = Order(
       id: json["id"],
       longitude: json["longitude"],
       latitude: json["latitude"],
       date: json["date"],
   );
   List<Map<String, dynamic>> res = json["products"];
   ord._products = List<Product>.from(res.map((x) => Product.fromMap(x)));
   return ord;
  }

  //remise à zéro de la commande pour permettre une autre
  reset(){
    _products = [];
    notifyListeners();
  }
}