import 'package:app/models/Order.dart';
import 'package:flutter/material.dart';
class History with ChangeNotifier{
  List<Order> _orders = [];

  Future<void> addOrder(cartProduct) async {
    Order ord = Order.fromMap(cartProduct);
    if(ord.products != null){
      _orders.insert(
        0,
       ord
      );
      notifyListeners();
    }
  }

  List<Order> get orders {
    return [..._orders];
  }


  Future<void> deleteOrder(String id) async {
    final existingOrderIndex = _orders.indexWhere((ord) => ord.id == id);
    _orders.removeAt(existingOrderIndex);
    notifyListeners();
  }
}