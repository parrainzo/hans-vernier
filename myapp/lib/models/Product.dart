import 'dart:convert';
class Product{
  final int id;
  final String name;
  final int price;
  final String imageUrl;

  Product({
    this.id,
    this.name,
    this.price,
    this.imageUrl,
  });

  Product copyWith({
    int id,
    String name,
    int price,
    String imageUrl,
  }) =>
      Product(
        id: id ?? this.id,
        name: name ?? this.name,
        price: price ?? this.price,
        imageUrl: imageUrl ?? this.imageUrl,
      );

  factory Product.fromJson(String str) => Product.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Product.fromMap(Map<String, dynamic> json) => Product(
    id: json["id"],
    name: json["name"],
    price: json["price"],
    imageUrl: json["image_url"],
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "name": name,
    "price": price,
    "image_url": imageUrl,
  };
}