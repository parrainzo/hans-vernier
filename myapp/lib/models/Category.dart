import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:app/models/Product.dart';

class Cat with ChangeNotifier{
  final int id;
  final String name;
  final List<Product> products;

  Cat({
    this.id,
    this.name,
    this.products,
  });

  Cat copyWith({
    int id,
    String name,
    List<Product> products,
  }) =>
      Cat(
        id: id ?? this.id,
        name: name ?? this.name,
        products: products ?? this.products,
      );

  factory Cat.fromJson(String str) => Cat.fromMap(json.decode(str));
  String toJson() => json.encode(toMap());

  factory Cat.fromMap(Map<String, dynamic> json) => Cat(
    id: json["id"],
    name: json["name"],
    products: List<Product>.from(json["products"].map((x) => Product.fromMap(x))),
  );
  Map<String, dynamic> toMap() => {
    "id": id,
    "name": name,
    "products": List<dynamic>.from(products.map((x) => x.toMap())),
  };
}