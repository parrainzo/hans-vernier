import 'package:flutter/foundation.dart';
import 'package:app/models/Category.dart';
import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class CategoryList with ChangeNotifier{
  List<Cat> _cats = [];
  List<Cat> get cats {
    //Ajouter la catégorie à la liste déjà existante - supporté par les dernières versions de dart
    return [..._cats];
  }

  //Recupération du JSON depuis les assets
  Future<String> _loadPhotoAsset() async {
    return await rootBundle.loadString('assets/json/data.json');
  }


  //Requête de demande de donnée pour l'affichage des catégories
  Future<List<Cat>> fetchCats() async {
    try {
      final response = await _loadPhotoAsset();
      final jsonResponse = json.decode(response) as List<dynamic>;
      List<Cat> res = new List<Cat>();
      res = jsonResponse.map((i)=>Cat.fromMap(i)).toList();
      print(res);
      _cats = res;
      notifyListeners();
      return _cats;
    } catch (error) {
      throw (error);
    }
  }
}