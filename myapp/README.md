# OrderDelivery

L'application que j'ai réalisé comporte 3 pages principales:

- Page d'accueil:
    - Possibilité de commander un plat selon une catégorie
    - Possibilité de d'annuler la commande du plat en cliquant une seconde fois sur lui.
    - Tout les plats commandées par l'utilisateur sont sotckées dans un panier temporaire
        en attendant qu'il complète sa commande avec sa position.
    - Pour ajouter la position, il suffit de cliquer sur le boutton "order now", après quoi une
        boite de dialogue s'ouvre à l'utiliateur pour lui donner la possibilité de choisir sa position.
- Page de maps:
    - Elle s'ouvre directement sur la position de l'utilisateur, pour facilité la recherge des lieux alentours.
    - Lorsque l'utilisateur a trouver la position à laquelle il souhaite se faire livrer, il lui
        suffit de maintenir sur l'écran à l'endroit voulu, un marqueur apparait alors et
        la commande est validée.
    - Après commande, retour à la page d'accueil.
- Page historique:
    - Affichage des différentes commandes de l'utilisateur avec la date (depuis lors).
    - Précision sur le lieu ou l'utilisateur a fait la commande (pour prouver que la position est bien recupérée
        à la commande 😉).
    -
